export const menus = [
  { name: "Home", link: "/" },
  {
    name: "About",
    link: "/about",
    children: [
      { name: "About NomaTech ICMSN", link: "/about" },
      { name: "Scientific Committee", link: "/about/scientific-committee" },
    ],
  },
  { name: "Speakers", link: "/speakers" },
  { name: "Program", link: "#" },
  { name: "Abstract Submission", link: "/submit-abstract" },
  { name: "Register", link: "/register" },
  {
    name: "Venue",
    link: "/venue",
    children: [
      { name: "Nearby hotels", link: "/venue/nearby-hotels" },
      { name: "Tourist Information", link: "/venue/tourist-information" },
      { name: "Airport shuttle", link: "https://forms.gle/sFGEtPeHeu7CUma59" },
    ],
  },
  { name: "Brochure", link: "#" },
];
