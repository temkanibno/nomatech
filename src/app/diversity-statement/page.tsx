import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";

const content = `
<p class="p1">At NomaTech ICMSN-2024, we are committed to fostering an inclusive and diverse environment that values and respects the contributions of all individuals. We believe that diversity enriches our collective experience, drives innovation, and enhances the quality of our interactions and outcomes.</p>
<p class="p1">We recognize the importance of representation and strive to ensure that our conference program reflects the rich diversity of our global community. We are dedicated to promoting equitable opportunities for participation and engagement across all demographic groups, including but not limited to gender, ethnicity, and socio-economic background.</p>
<p class="p1">As part of our commitment to diversity and inclusion, we uphold the following principles:</p>
<p class="p2">1. Representation: We are committed to achieving representation and involvement of the entire community in our conference activities. We seek to create a program that reflects the diversity of perspectives, experiences, and expertise within our field.</p>
<p class="p2">2. Inclusivity: We are dedicated to creating an inclusive environment where all attendees feel welcome, valued, and respected. We strive to foster friendly and respectful interactions among participants, free from discrimination, harassment, or bias.</p>
<p class="p3">3. Transparency: We are committed to transparency in our processes and decision-making related to diversity and inclusion. We welcome feedback and input from our community to help us continuously improve and evolve our practices.</p>
<p class="p4">By embracing diversity and inclusion, we aim to create a vibrant and dynamic conference experience that empowers all attendees to learn, connect, and contribute to the advancement of knowledge in our field.</p>`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Diversity Statement" items={[{ title: "Home", href: "/" }, { title: "Diversity Statement" }]} />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-8">
          <div className="col-span-2" />
          <div className="col-span-8">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
          </div>
          <div className="col-span-2" />
        </div>
      </div>
    </main>
  );
}
