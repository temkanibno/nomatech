import { AboutUs, Carousels, Participants, Reasons, Speakers, Symposiums } from "@nano/components/home";
import { Golden } from "@nano/components/home/Golden";
import { CoOrg } from "@nano/components/home/CoOrg";
import { MainOrganizers } from "@nano/components/home/MainOrganizers";
import { Sponsors } from "@nano/components/home/Sponsors";
import { Supporting } from "@nano/components/home/Supporting";

export default function Home() {
  return (
    <>
      <Carousels />
      <AboutUs />
      <Speakers />
      <Symposiums />
      <Reasons />
      <MainOrganizers />
      <CoOrg />
      <Golden />
      <Sponsors />
      <Supporting />
      <Participants />
    </>
  );
}
