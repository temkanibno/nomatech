import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";

const content = `
<h2><strong>National University of Mongolia</strong></h2>
<p>
<figure>
<img src="https://news.num.edu.mn/wp-content/uploads/2015/01/muis2.png" alt="National university of mongolia"/>
</figure>
</p>
<p><b>Location:</b> Ikh surguuliin gudami-1, Baga toiruu, Sukhbaatar district, P.O.BOX-46A/523, 14201, Ulaanbaatar, Mongolia</p>
<p>Google map:</p>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2673.690011739576!2d106.9188949!3d47.923035199999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d9692422e995f0d%3A0x5a0c476857758c27!2sNational%20University%20of%20Mongolia!5e0!3m2!1sen!2smn!4v1715700564042!5m2!1sen!2smn"
              width="100%"
              height="450"
              allowFullScreen
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            ></iframe>
            
`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Venue" items={[{ title: "Home", href: "/" }, { title: "Venue" }]} />
      <div className="container md:my-10 my-5">
        <div className="grid md:grid-cols-12 gap-8">
          <div className="col-span-2" />
          <div className="col-span-8">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
          </div>
          <div className="col-span-2" />
        </div>
      </div>
    </main>
  );
}
