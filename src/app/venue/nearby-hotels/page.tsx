import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";
import { HotelAlbum } from "./album";
import Link from "next/link";
import { nanoid } from "nanoid";

const content = `
<h2><strong>Nearby Hotels for Conference Attendees</strong></h2>
`;

const hotels = [
  {
    name: "Shangri-la Ulaanbaatar",
    rating: 5,
    photos: [
      {
        height: 768,
        width: 1024,
        src: "https://cf.bstatic.com/xdata/images/hotel/max1024x768/187230691.jpg?k=81b6e29cedee962ac1a4d609b337d57e440f85cda1e953e52807c7ab0a071196&o=&hp=1",
      },
      {
        height: 900,
        width: 573,
        src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/295689361.jpg?k=fe68e22053cf1101411f3b09a678e1e215ce2adf6b8e37d3cfd4d176fb866120&o=&hp=1",
      },
      {
        height: 900,
        width: 1232,
        src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/151497029.jpg?k=147376a958938bbed61eda443078d0ca68b9116a33267ba0e8effe6c2dd7a893&o=&hp=1",
      },
      {
        height: 900,
        width: 1200,
        src: "https://cf.bstatic.com/xdata/images/hotel/max1280x900/188554504.jpg?k=fffe1882d3f317bed8856c89483568b398f8d372a241abbcf7c53ecee1fe29a7&o=&hp=1",
      },
    ],
    distance: "1km",
    prices: {
      single: "1,154,825-1,494,479₮",
      double: "1,256,721-1,596,376₮",
    },
    link: "https://www.booking.com/hotel/mn/shangri-la-ulaanbaatar.en-gb.html",
  },
  {
    name: "Best Western Premier Tuushin Hotel",
    rating: 5,
    photos: [],
    distance: "240m",
    prices: {
      single: "825,563-1,522,603₮",
      double: "978,408-24,455,116₮",
    },
    link: "https://www.booking.com/hotel/mn/best-western-premier-tuushin-ulaanbaatar.en-gb.html",
  },
  {
    name: "The Blue Sky Hotel and Tower",
    rating: 5,
    distance: "650m",
    prices: {
      single: "594,395-747,240₮",
      double: "543,477-1,426,548₮",
    },
    link: "https://www.booking.com/hotel/mn/blue-sky-ulaanbaatar.en-gb.html?label=gen173nr-1FCAsolgFCKGJlc3Qtd2VzdGVybi1wcmVtaWVyLXR1dXNoaW4tdWxhYW5iYWF0YXJIM1gEaJYBiAEBmAEJuAEHyAEN2AEB6AEB-AELiAIBqAIDuAL044yyBsACAdICJDI2NjAzY2I0LWMxYzMtNGI2Ni04MWEzLWMyMzY2MDM0N2JhNtgCBuACAQ&sid=f04d245861648afc00f167753596aad3&aid=304142&ucfs=1&arphpl=1&checkin=2024-05-19&checkout=2024-05-20&dest_id=-2353539&dest_type=city&group_adults=2&req_adults=2&no_rooms=1&group_children=0&req_children=0&hpos=12&hapos=12&sr_order=popularity&srpvid=e1b044d27520026c&srepoch=1715680061&all_sr_blocks=45647019_0_2_0_0&highlighted_blocks=45647019_0_2_0_0&matching_block_id=45647019_0_2_0_0&sr_pri_blocks=45647019_0_2_0_0__13580&from_sustainable_property_sr=1&from=searchresults",
  },
  {
    name: "Novotel Ulaanbaatar",
    rating: 4,
    distance: "500m",
    prices: {
      single: "434,758-730,257₮",
      double: "485,706-781,205₮",
    },
    link: "https://www.booking.com/hotel/mn/novotel-ulaanbaatar.en-gb.html?aid=304142&label=gen173nr-1FCAsolgFCKGJlc3Qtd2VzdGVybi1wcmVtaWVyLXR1dXNoaW4tdWxhYW5iYWF0YXJIM1gEaJYBiAEBmAEJuAEHyAEN2AEB6AEB-AELiAIBqAIDuAL044yyBsACAdICJDI2NjAzY2I0LWMxYzMtNGI2Ni04MWEzLWMyMzY2MDM0N2JhNtgCBuACAQ&sid=0259929b00004a3abbfa37837a00dfc2&all_sr_blocks=367867902_119414447_2_42_0;checkin=2024-05-19;checkout=2024-05-20;dist=0;group_adults=2;group_children=0;hapos=8;highlighted_blocks=367867902_119414447_2_42_0;hpos=8;matching_block_id=367867902_119414447_2_42_0;no_rooms=1;req_adults=2;req_children=0;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=367867902_119414447_2_42_0__15500;srepoch=1715680352;srpvid=d8cd456cbae0013d;type=total;ucfs=1&",
  },
  {
    name: "Bishrelt Hotel",
    rating: 3,
    distance: "1km",
    prices: {
      single: "373,620-407,585₮",
      double: "304,602-458,533₮",
    },
    link: "https://www.booking.com/hotel/mn/bishrelt.en-gb.html?aid=304142&label=gen173nr-1FCAsolgFCKGJlc3Qtd2VzdGVybi1wcmVtaWVyLXR1dXNoaW4tdWxhYW5iYWF0YXJIM1gEaJYBiAEBmAEJuAEHyAEN2AEB6AEB-AELiAIBqAIDuAL044yyBsACAdICJDI2NjAzY2I0LWMxYzMtNGI2Ni04MWEzLWMyMzY2MDM0N2JhNtgCBuACAQ&sid=0259929b00004a3abbfa37837a00dfc2&all_sr_blocks=147107304_127544554_2_1_0;checkin=2024-05-19;checkout=2024-05-20;dist=0;group_adults=2;group_children=0;hapos=9;highlighted_blocks=147107304_127544554_2_1_0;hpos=9;matching_block_id=147107304_127544554_2_1_0;no_rooms=1;req_adults=2;req_children=0;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=147107304_127544554_2_1_0__9500;srepoch=1715680387;srpvid=54bb457a2cfd01bb;type=total;ucfs=1&",
  },
  {
    name: "Millennium Plaza Hotel",
    rating: 4,
    distance: "1.3km",
    prices: {
      single: "577,412-1,345,031₮",
      double: "565,525-1,395,980₮",
    },
    link: "https://www.booking.com/hotel/mn/millennium-plaza-ulaanbaatar.en-gb.html?aid=304142&label=gen173nr-1FCAsolgFCKGJlc3Qtd2VzdGVybi1wcmVtaWVyLXR1dXNoaW4tdWxhYW5iYWF0YXJIM1gEaJYBiAEBmAEJuAEHyAEN2AEB6AEB-AELiAIBqAIDuAL044yyBsACAdICJDI2NjAzY2I0LWMxYzMtNGI2Ni04MWEzLWMyMzY2MDM0N2JhNtgCBuACAQ&sid=0259929b00004a3abbfa37837a00dfc2&all_sr_blocks=853923303_359760255_2_1_0;checkin=2024-05-19;checkout=2024-05-20;dist=0;group_adults=2;group_children=0;hapos=28;highlighted_blocks=853923303_359760255_2_1_0;hpos=3;matching_block_id=853923303_359760255_2_1_0;no_rooms=1;req_adults=2;req_children=0;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=853923303_359760255_2_1_0__13000;srepoch=1715680419;srpvid=a7944589ff6301e5;type=total;ucfs=1&",
  },
  {
    name: "H9 Hotel Nine",
    rating: 3,
    distance: "260m",
    prices: {
      single: "230,965-373,620₮",
      double: "346,447-424,568₮",
    },
    link: "https://www.booking.com/hotel/mn/h9-nine-ulaanbaatar-ulaanbaatar2.en-gb.html?label=gen173nr-1FCAsolgFCKGJlc3Qtd2VzdGVybi1wcmVtaWVyLXR1dXNoaW4tdWxhYW5iYWF0YXJIM1gEaJYBiAEBmAEJuAEHyAEN2AEB6AEB-AELiAIBqAIDuAL044yyBsACAdICJDI2NjAzY2I0LWMxYzMtNGI2Ni04MWEzLWMyMzY2MDM0N2JhNtgCBuACAQ&sid=f04d245861648afc00f167753596aad3&aid=304142&ucfs=1&arphpl=1&checkin=2024-05-19&checkout=2024-05-20&group_adults=2&req_adults=2&no_rooms=1&group_children=0&req_children=0&hpos=10&hapos=10&sr_order=popularity&srpvid=165245b09f3600d6&srepoch=1715680519&all_sr_blocks=138028602_390671129_2_1_0&highlighted_blocks=138028602_390671129_2_1_0&matching_block_id=138028602_390671129_2_1_0&sr_pri_blocks=138028602_390671129_2_1_0__10200&from_sustainable_property_sr=1&from=searchresults",
  },
  {
    name: "Platinum Hotel",
    rating: 4,
    distance: "800m",
    prices: {
      double: "213,982-311,361₮",
    },
    link: "https://www.booking.com/hotel/mn/platinum.en-gb.html?label=gen173nr-1FCAsolgFCKGJlc3Qtd2VzdGVybi1wcmVtaWVyLXR1dXNoaW4tdWxhYW5iYWF0YXJIM1gEaJYBiAEBmAEJuAEHyAEN2AEB6AEB-AELiAIBqAIDuAL044yyBsACAdICJDI2NjAzY2I0LWMxYzMtNGI2Ni04MWEzLWMyMzY2MDM0N2JhNtgCBuACAQ&sid=f04d245861648afc00f167753596aad3&aid=304142&ucfs=1&arphpl=1&checkin=2024-05-19&checkout=2024-05-20&dest_id=-2353539&dest_type=city&group_adults=2&req_adults=2&no_rooms=1&group_children=0&req_children=0&hpos=14&hapos=14&sr_order=popularity&srpvid=592e468e914a03d2&srepoch=1715680937&all_sr_blocks=101021506_169646184_2_2_0&highlighted_blocks=101021506_169646184_2_2_0&matching_block_id=101021506_169646184_2_2_0&sr_pri_blocks=101021506_169646184_2_2_0__4950&from_sustainable_property_sr=1&from=searchresults",
  },
  {
    name: "Springs Hotel",
    rating: 3,
    distance: "750m",
    prices: {
      single: "256,137-477,964₮",
      double: "258,379-660,045₮",
    },
    link: "https://www.booking.com/hotel/mn/springs-in-ulaanbaatar.en-gb.html?aid=304142&label=gen173nr-1FCAsolgFCKGJlc3Qtd2VzdGVybi1wcmVtaWVyLXR1dXNoaW4tdWxhYW5iYWF0YXJIM1gEaJYBiAEBmAEJuAEHyAEN2AEB6AEB-AELiAIBqAIDuAL044yyBsACAdICJDI2NjAzY2I0LWMxYzMtNGI2Ni04MWEzLWMyMzY2MDM0N2JhNtgCBuACAQ&sid=0259929b00004a3abbfa37837a00dfc2&all_sr_blocks=101427401_0_2_1_0;checkin=2024-05-19;checkout=2024-05-20;dist=0;group_adults=2;group_children=0;hapos=13;highlighted_blocks=101427401_0_2_1_0;hpos=13;matching_block_id=101427401_0_2_1_0;no_rooms=1;req_adults=2;req_children=0;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=101427401_0_2_1_0__7606;srepoch=1715680536;srpvid=165245b09f3600d6;type=total;ucfs=1&",
  },
];

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Nearby hotels" items={[{ title: "Home", href: "/" }, { title: "Nearby hotels" }]} />
      <div className="container md:my-10 my-5">
        <div className="grid md:grid-cols-12 gap-8">
          <div className="col-span-2" />
          <div className="col-span-8">
            <div className="content">
              {<div dangerouslySetInnerHTML={{ __html: content }} />}
              {hotels.map((hotel) => (
                <Hotel key={nanoid()} {...hotel} />
              ))}
              <h5>Hotels above 1.5 km radius</h5>
              <ul>
                <li>Ramada Ulaanbaatar City Center</li>
                <li>Bayangol Hotel</li>
                <li>The Corporate Hotel</li>
                <li>Chinggis Khaan Hotel</li>
                <li>Nomado Boutique Hotel</li>
                <li>Kempinski Hotel Khan Palace</li>
                <li>Flower Hotel Ulaanbaatar</li>
                <li>Continental Hotel</li>
              </ul>
            </div>
          </div>
          <div className="col-span-2" />
        </div>
      </div>
    </main>
  );
}
const Stars = ({ rating }: { rating: number }) => {
  const stars = "★".repeat(rating);

  return <span className="text-[#ffcd3c]">{stars}</span>;
};

interface Photo {
  height: number;
  width: number;
  src: string;
}

interface Hotel {
  name: string;
  rating: number;
  distance: string;
  prices: {
    single?: string;
    double?: string;
  };
  photos?: Photo[];
  link: string;
}

const Hotel: React.FC<Hotel> = ({ name, rating, distance, prices, photos, link }) => {
  return (
    <div>
      <h5>
        {name} <Stars rating={rating} />
      </h5>
      {photos && <HotelAlbum photos={photos} />}
      <p>
        <b>Distance from Venue:</b> {distance}
      </p>
      <p>
        {prices.single && (
          <>
            <b>1 person</b> - {prices.single} <br />
          </>
        )}
        <b>2 persons</b> - {prices.double}
      </p>
      <p>
        <Link href={link} target="_blank">
          For more information
        </Link>
      </p>
    </div>
  );
};
