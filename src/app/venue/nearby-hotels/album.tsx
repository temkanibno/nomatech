"use client";
import { useState } from "react";
import PhotoAlbum from "react-photo-album";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";

// import optional lightbox plugins
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import "yet-another-react-lightbox/plugins/thumbnails.css";

// Define the type for a photo
interface Photo {
  src: string;
  width: number;
  height: number;
}

// Define the type for the component props
interface AlbumProps {
  photos: Photo[];
}

export const HotelAlbum: React.FC<AlbumProps> = ({ photos }) => {
  const [index, setIndex] = useState(-1);
  return (
    <>
      <PhotoAlbum layout="rows" photos={photos} onClick={({ index }) => setIndex(index)} />
      <Lightbox slides={photos} open={index >= 0} index={index} close={() => setIndex(-1)} plugins={[Fullscreen, Slideshow, Thumbnails, Zoom]} />
    </>
  );
};
