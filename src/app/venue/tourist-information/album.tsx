"use client";
import { useState } from "react";
import PhotoAlbum from "react-photo-album";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";

// import optional lightbox plugins
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Slideshow from "yet-another-react-lightbox/plugins/slideshow";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import "yet-another-react-lightbox/plugins/thumbnails.css";
const photos = [
  {
    src: "/gallery/1.jpeg",
    width: 1920,
    height: 1280,
  },
  {
    src: "/gallery/2.jpeg",
    width: 1588,
    height: 960,
  },
  {
    src: "/gallery/3.jpeg",
    width: 1200,
    height: 799,
  },
  {
    src: "/gallery/4.jpeg",
    width: 1445,
    height: 961,
  },
  {
    src: "/gallery/5.jpeg",
    width: 1445,
    height: 959,
  },
  {
    src: "/gallery/6.jpeg",
    width: 1506,
    height: 964,
  },
];

export const Album = () => {
  const [index, setIndex] = useState(-1);
  return (
    <>
      <PhotoAlbum layout="rows" photos={photos} onClick={({ index }) => setIndex(index)} />
      <Lightbox slides={photos} open={index >= 0} index={index} close={() => setIndex(-1)} plugins={[Fullscreen, Slideshow, Thumbnails, Zoom]} />
    </>
  );
};
