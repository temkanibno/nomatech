import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";
import { Album } from "./album";

const content = `
<p><strong>Tourist Information</strong></p>
<p><span style="font-weight: 400;">As we gear up for the upcoming NomaTech ICMSN-2024 for Material Science and Nanotechnology, we extend a warm invitation to you to explore the boundless wonders of Mongolia. Nestled between Russia and China, Mongolia boasts a rich tapestry of culture, history, and natural beauty, offering a unique backdrop for scholarly exchange and exploration. Below, we've listed well-known tourist companies for your information, but please note that NomaTech ICMSN-2024 is not responsible for any travel-related matters.</span></p>
<ul>
<li><strong><strong>Mongolia Quest</strong></strong></li>
</ul>
<p><strong>Website:</strong><span style="font-weight: 400;"> https://mongoliaquest.com</span></p>
<p><strong>Email:</strong><span style="font-weight: 400;"> info@MongoliaQuest.com</span></p>
<p><strong>Tel: </strong><span style="font-weight: 400;">+976 7000 9747</span></p>
<p><strong>Description: </strong><span style="font-weight: 400;">With a legacy spanning three decades, Mongolia Quest is a household name in the Mongolian travel market. Our pioneering spirit has led us to establish connections with the best herders, community groups, and conservation organizations, enabling us to offer unique experiences for individual travelers and large groups alike. Join us to explore Mongolia and beyond with unparalleled expertise and adventure.</span></p>
<ul>
<li><strong><strong>Tour Mongolia</strong></strong></li>
</ul>
<p><strong>Website:</strong><span style="font-weight: 400;"> https://tourmongolia.com</span></p>
<p><strong>Email:</strong> <span style="font-weight: 400;">info@tourmongolia.com</span></p>
<p><strong>Tel: </strong><span style="font-weight: 400;">+976 11-354662</span></p>
<p><strong>Description: </strong><span style="font-weight: 400;">Founded with passion and experience, Tour Mongolia offers authentic and eco-friendly adventures. With a network of local stakeholders, including nomadic communities and multilingual guides, we create customized tours that prioritize sustainability and cultural immersion. Join us to explore Mongolia responsibly while supporting local communities.</span></p>
<ol>
<li style="font-weight: 400;"><span style="font-weight: 400;">Mongolian Guide Tour</span></li>
</ol>
<p><strong>Website:</strong><span style="font-weight: 400;"> www.touristinfocenter.mn</span></p>
<p><strong>Email:</strong><span style="font-weight: 400;"> info@touristinfocenter.mn</span></p>
<p><strong>Tel: </strong><span style="font-weight: 400;">+976 7010 1011</span></p>
<p><strong>Description:</strong> <span style="font-weight: 400;">Mongolian Guide Tour LLC, established in 2006 and based in Ulaanbaatar, is a trusted tour operator specializing in inbound and outbound tours. We offer tailor-made tours led by certified guides from our renowned Guide Education Center. Experience premier hiking, adventure, and group tours with our expert team.</span></p>

<ul>
<li><strong><span style="font-weight: 400;">Let's travel</span></strong></li>
</ul>

<p><strong>Website:</strong><span style="font-weight: 400;"> www.letstravelmongolia.com</span></p>
<p><strong>Email:</strong><span style="font-weight: 400;"> info@letstravel.mn</span></p>
<p><strong>Tel: </strong><span style="font-weight: 400;">+976 7000 2232</span></p>
<p><strong>Description:</strong><span style="font-weight: 400;"> Founded in 2006, Let&rsquo;s Travel Ltd Co. is one of Mongolia&rsquo;s leading tour operators, offering high-quality services including charters, package tours, leisure and corporate travels. Our experienced, multilingual team ensures top-notch travel experiences. With branch offices in major cities worldwide, we provide excellent value and reliability, helping customers explore the best of Mongolia and beyond.</span></p>
<ul>
<li><strong><strong>Discover Mongolia</strong></strong></li>
</ul>
<p><strong>Website:</strong> <a href="http://www.discovermongolia.mn"><span style="font-weight: 400;">www.discovermongolia.mn</span></a></p>
<p><strong>Email:</strong> <a href="mailto:info@discovermongolia.mn"><span style="font-weight: 400;">info@discovermongolia.mn</span></a></p>
<p><strong>Tel: </strong><a href="tel:976%207012%200011"><span style="font-weight: 400;">+ 976 7012 0011</span></a></p>
<p><strong>Description: </strong><span style="font-weight: 400;">Since 2004, Discover Mongolia! has been crafting unforgettable adventures in Mongolia with expertise and passion. We rebranded to better reflect our mission of helping travelers explore the beauty and culture of Mongolia. From bespoke tours to carefully selected partners, we ensure a premier travel experience led by our dedicated team in Ulaanbaatar.</span></p>
<ul>
<li><strong><strong>Zendmen Travel Mongolia</strong></strong></li>
</ul>
<p><strong>Website:</strong><span style="font-weight: 400;"> www.zendmentravel.com</span></p>
<p><strong>Email:</strong><span style="font-weight: 400;"> info@zendmentravel.com</span></p>
<p><strong>Tel: </strong><span style="font-weight: 400;">&nbsp;+976 9999 8015, +976 99086302, +976 70123242</span></p>
<p><strong>Description:</strong> <span style="font-weight: 400;">Zendmen Travel is a premier travel agency dedicated to providing unforgettable experiences for adventurers worldwide. Our expert team designs personalized itineraries, ensuring each journey is unique and tailored to your interests.</span></p>
<ul>
<li><strong><strong>View Mongolia Travel</strong></strong></li>
</ul>
<p><strong>Website:</strong><span style="font-weight: 400;"> www.viewmongolia.com</span></p>
<p><strong>Email:</strong><span style="font-weight: 400;"> viewmongolia@gmail.com</span></p>
<p><strong>Tel: </strong><span style="font-weight: 400;">+976 90099908</span></p>
<p><strong>Description:</strong> <span style="font-weight: 400;">Experience Mongolia with View Mongolia Travel and Tours, a fully Mongolian-owned company with over 10 years of expertise. We offer custom, private, and group tours to explore Mongolia&rsquo;s beauty. Let us be your trusted guide for an unforgettable adventure.</span></p>
<ul>
<li><strong><strong>Goyo Travel</strong></strong></li>
</ul>
<p><strong>Website:</strong><span style="font-weight: 400;"> www.goyotravel.com</span></p>
<p><strong>Email:</strong> <a href="mailto:info@goyotravel.com"><strong>info@goyotravel.com</strong></a></p>
<p><strong>Tel: </strong><span style="font-weight: 400;">+441869866520, +97699598468</span></p>
<p><strong>Description:</strong><span style="font-weight: 400;"> Goyo Travel is a specialist Mongolia tour operator offering organized group tours, private tailor-made trips, and additional travel services. Owned by British-Mongolian couple Oliver and Goyo Reston, with over 18 years of industry experience, we focus on staff excellence and personalized service to ensure an unforgettable journey through Mongolia. Our UK-registered company also manages operations in Mongolia to provide seamless travel experiences.</span></p>`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Tourist Information" items={[{ title: "Home", href: "/" }, { title: "Tourist Information" }]} />
      <div className="container md:my-10 my-5">
        <div className="grid md:grid-cols-12 gap-8">
          <div className="col-span-2" />
          <div className="col-span-8">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
            <Album />
          </div>
          <div className="col-span-2" />
        </div>
      </div>
    </main>
  );
}
