import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";

const content = `
Updated soon...`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Privacy policy" items={[{ title: "Home", href: "/" }, { title: "Privacy policy" }]} />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-8">
          <div className="col-span-2" />
          <div className="col-span-8">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
          </div>
          <div className="col-span-2" />
        </div>
      </div>
    </main>
  );
}
