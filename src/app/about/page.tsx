import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";

const content = `
<h2><em><span style="font-weight: 700;">NomaTech ICMSN-2024</span></em></h2>
<p><span style="font-weight: 400;">The core objectives of the &ldquo;NomaTech ICMSN-2024&rdquo; are to delve deep into the most recent innovations, research, and applications in both material science and nanotechnology, showcasing the forefront of global scientific advancements and also playing a pivotal role in amplifying local collaborations within Mongolia. By providing a platform where Mongolian researchers, institutions, and industry experts could interact directly with international counterparts, the conference has fostered an environment conducive to collaborative partnerships.</span></p>
<p><span style="font-weight: 400;">We are thrilled to extend a cordial invitation to researchers, experts, and innovators from across the globe to join us for the inaugural International Conference on Materials Science &amp; Nanotechnology. This three-day scientific gathering is set to take place in the heart of Ulaanbaatar, Mongolia, from July 8 to 9, 2024.</span></p>
<p><strong>Official Language:</strong><span style="font-weight: 400;"> The official language of the conference is English. Please note that translation and interpreting services will not be available.&nbsp;&nbsp;</span></p>
<p><strong>Weather in Ulaanbaatar:</strong><span style="font-weight: 400;"> Experience the unique climate of Ulaanbaatar during the conference. In July, the temperatures are generally moderate, offering a pleasant atmosphere. Expect daytime temperatures around 73&deg;F (23&deg;C) and nighttime lows at approximately 59&deg;F (15&deg;C).</span></p>
<p><strong>Special Instructions:</strong><span style="font-weight: 400;">&nbsp;&nbsp;</span></p>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Please arrive at the conference room at least 30 minutes before your session begins.&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Any changes to the conference program will be communicated to participants in a timely manner.&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Free Wi-Fi is available in the conference room for your convenience.&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">For safety reasons, electrical outlets will not be available for use.&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">To respect speakers and fellow participants, kindly turn your mobile phones to silent before entering the sessions.&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">The event will be photographed. If you are taking photos, please turn off your flash.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">The distribution of advertising messages in any form is strictly prohibited, except for exhibitors/sponsors.</span></li>
</ul>
<p><strong>Principal Organizers</strong></p>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Center for Nanoscience and Nanotechnology, National University of Mongolia&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Institute of Physics and Physical Technology, Mongolian Academy of Sciences&nbsp;</span></li>
</ul>
<p><span style="font-weight: 400;">We look forward to your active participation and the valuable contributions you'll bring to the </span><strong>NomaTech ICMSN-2024</strong><span style="font-weight: 400;">!!!</span></p>`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="About us" items={[{ title: "Home", href: "/" }, { title: "About us" }]} />
      <div className="container md:my-10 my-5">
        <div className="grid md:grid-cols-12 gap-8">
          <div className="col-span-2 md:block hidden" />
          <div className="col-span-8">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
          </div>
          <div className="col-span-2 md:block hidden" />
        </div>
      </div>
    </main>
  );
}
