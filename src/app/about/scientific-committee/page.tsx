import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";

const content = `
<table>
<tbody>
<tr>
<td>
<p><span style="font-weight: 400;">Battogtokh Dorjgotov</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Ministry of Education and Science</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Odgerel Dorjgochoo</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Mongolian Foundation for Science and Technology</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Ganzorig Chimed</span></p>
</td>
<td>
<p><span style="font-weight: 400;">National University of Mongolia</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Davaasambuu Jav</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Institute of Physics and Technology, Mongolian Academy of Sciences&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Batjargal Batdorj</span></p>
</td>
<td>
<p><span style="font-weight: 400;">National University of Mongolia</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Avid Budeebazar</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Mongolian Academy of Sciences</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Jargalan Purevsuren</span></p>
</td>
<td>
<p><span style="font-weight: 400;">National Defense University</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Gantulga Davaakhuu</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Institute of Biology, Mongolian Academy of Sciences</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Namnan Tumurpurev</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Mongolian University of Science and Technology</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Damdindorj Boldbaatar</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Mongolian National University of Medical Sciences</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Baasansukh Badarch</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Mongolian University of Life Sciences</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Battsengel Baatar</span></p>
</td>
<td>
<p><span style="font-weight: 400;">German-Mongolian Institute for Resources and Technology</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Jamiyanaa Dashdorj</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Chatham University</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Battogtokh Jugdersuren</span></p>
</td>
<td>
<p><span style="font-weight: 400;">U.S. Naval Research Laboratory</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Odkhuu Dorj&nbsp;</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Incheon National University</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Ganbaatar Tumen-Ulzii</span></p>
</td>
<td>
<p><span style="font-weight: 400;">University of Cambridge</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Munkhbayar Batmunkh</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Griffith University&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Battulga Munkhbat</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Technical University of Denmark</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Chinzorig Bavuu</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Mongolian National Mining Association</span></p>
</td>
</tr>
</tbody>
</table>`;

export default function About() {
  return (
    <main>
      <Breadcrumbs
        title="Scientific Committee"
        items={[{ title: "Home", href: "/" }, { title: "About", href: "/about" }, { title: "Scientific Committee" }]}
      />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-8">
          <div className="md:col-span-2" />
          <div className="md:col-span-8 col-span-12">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
          </div>
          <div className="md:col-span-2" />
        </div>
      </div>
    </main>
  );
}
