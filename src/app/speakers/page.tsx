import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";
import { SpeakerCard } from "@nano/components/core/SpeakerCard";
import { mainSpeakers, speakers } from "@nano/utils/speakers";
import { nanoid } from "nanoid";

const content = `
`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Speakers" items={[{ title: "Home", href: "/" }, { title: "Speakers" }]} />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-8">
          <div className="col-span-2 md:block hidden" />
          <div className="md:col-span-8 col-span-12">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
            <div className="grid md:grid-cols-4 sm:grid-cols-2 grid-cols-1">
              {mainSpeakers.map((speaker) => (
                <SpeakerCard plenary speaker={speaker} key={nanoid()} />
              ))}
              {speakers.map((speaker) => (
                <SpeakerCard small={true} speaker={speaker} key={nanoid()} />
              ))}
            </div>
          </div>
          <div className="col-span-2 md:block hidden" />
        </div>
      </div>
    </main>
  );
}
