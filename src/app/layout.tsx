import type { Metadata } from "next";
import { Livvic } from "next/font/google";
import "./globals.css";
import { Footer, Header } from "@nano/components/layout";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const livvic = Livvic({ subsets: ["latin"], weight: ["400", "500", "700"] });

export const metadata: Metadata = {
  title: "NomaTech ICMSN-2024",
  description: "NomaTech ICMSN-2024 - NOMATECH FIRST INTERNATIONAL CONFERENCE ON MATERIALS SCIENCE AND NANOTECHNOLOGY",
  openGraph: {
    title: "NomaTech ICMSN-2024",
    description: "NomaTech ICMSN-2024 - NOMATECH FIRST INTERNATIONAL CONFERENCE ON MATERIALS SCIENCE AND NANOTECHNOLOGY",
    type: "website",
    url: "https://www.nomatech.mn",
    images: [{ url: "https://www.nomatech.mn/images/2.webp" }],
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png" />
        <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/manifest.json" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
        <meta name="theme-color" content="#ffffff" />
      </head>
      <body className={`${livvic.className} max-w-screen overflow-x-hidden`}>
        <Header />
        <main>{children}</main>
        <Footer />
        <ToastContainer position="bottom-right" />
      </body>
    </html>
  );
}
