"use client";
import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";
import { Button } from "@nano/components/core/Button";
import { Input } from "@nano/components/core/Input";
import { Select } from "@nano/components/core/Select";
import { countries } from "@nano/utils/countries";
import { useFormik } from "formik";
import * as Yup from "yup";

import Link from "next/link";
import { FileUpload } from "@nano/components/core/FileUpload";
import { useState } from "react";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";

const honorifics = ["Dr.", "Mr.", "Mrs.", "Miss", "Ms.", "Professor"];
const interestedIns = ["Oral Presentation", "Poster Presentation", "Others"];
const content = `<p><strong>Abstract submission guidelines</strong><span style="font-weight: 400;">&nbsp;</span></p>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">The Abstract of *up to 300 words*, in *English*, can summarize the main aspects of the presentation, from the overall purpose of the study, research topic(s) explored, methodology (techniques used, time and place, etc.) as well as findings the main (results) or trends found as a result of the study.&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">All abstract will be only submitted by online system.&nbsp;</span></li>
</ul>`;
const topics = `
<p><strong>SESSION 1:NEXT-GENERATION ADVANCED MATERIALS: FUNDAMENTALS & APPLICATIONS</strong></p>
<ul>
<li><strong><span style="font-weight: 400;">Advanced Materials Science - Innovative Experimental and Computational Modeling Approaches and Applications</span></strong></li>
<li><span style="font-weight: 400;">Green Materials for Environment and Sustainable Development</span></li>
<li><span style="font-weight: 400;">Materials in Optoelectronics - Materials, Fundamentals and Applications&nbsp;</span></li>
<li><span style="font-weight: 400;">Emerging Opto-Magnetic Materials - Advances, Trends and Challenges</span></li>
<li><span style="font-weight: 400;">Emerging Functional Materials: Surfaces and Interfaces in Electronics and Photonics</span></li>
<li><span style="font-weight: 400;">Advanced Coatings and Surface Engineering</span></li>
<li><span style="font-weight: 400;">Additive Engineering and 3D Printing</span></li>
<li><span style="font-weight: 400;">Materials Chemistry and Engineering</span>&nbsp;</li>
</ul>
<p><strong>SESSION 2: EMERGING FUNCTIONAL MATERIALS & DEVICES</strong></p>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Advanced Materials: Nanocomposites, Nanostructures, and Nanoporous Substances&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Functional Nanomaterials: Design, Characteristics, and Applications&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Nanomaterials for Energy and Environmental Applications</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Nanomaterials for Electronics and Photonics</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Graphene and Carbon Nanostructures</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Nanotechnology for Biodegradable Materials and Green Processes</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Nanofluids: Applications and Advancements</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Mineralogy, Crystallography, and Structural Complexity</span></li>
</ul>
<p><strong>SESSION 3: NOVEL TRENDS IN NANOSTRUCTURES & NANOMATERIALS: DESIGN, CHARACTERISTICS, & APPLICATIONS</strong></p>
<ul>
<li><strong><span style="font-weight: 400;">Biomaterials and Biomedical Materials</span></strong></li>
<li><span style="font-weight: 400;">Bioinspired Structural Composites - Advances in Experiments, Simulations and AI-Based Design</span></li>
<li><span style="font-weight: 400;">Nanomedicine and Drug Delivery</span></li>
<li><span style="font-weight: 400;">Advanced Soft Materials for Bioelectronic Interfaces</span></li>
<li><span style="font-weight: 400;">Smart Biomaterials and Bioresponsive Materials</span></li>
<li><span style="font-weight: 400;">Sustainable Agriculture, Food Security, and Forestry</span></li>
</ul>
<p><strong>SESSION 4: CURRENT AND FUTURE PROSPECTS OF BIO & NANOTECHNOLOGY</strong>&nbsp;</p>
<ul>
<li><strong><span style="font-weight: 400;">Extraction, Processing, Recycling, and Manufacturing &ndash; Natural and Biological Resources&nbsp;</span></strong></li>
<li><span style="font-weight: 400;">Powder Metallurgy and Geometallurgy &ndash; Science, Technology and Applications</span></li>
<li><span style="font-weight: 400;">New Technologies for Utilization of Minerals and Metal Containing Resources</span></li>
<li><span style="font-weight: 400;">Global Environmental Protection &ndash; Extraction of Raw Materials and Resource Use</span></li>
</ul>
`;

export default function About() {
  const router = useRouter();
  const [file, setFile] = useState<File | null>(null);

  const validationSchema = Yup.object().shape({
    title: Yup.string().required("Title is required"),
    name: Yup.string().required("Name is required"),
    email: Yup.string().email("Invalid email address").required("Email is required"),
    organization: Yup.string().required("Organization is required"),
    telephone: Yup.string().required("Telephone is required"),
    country: Yup.string().required("Country is required"),
    interestedIn: Yup.string().required("Interested in is required"),
    abstractTitle: Yup.string().required("Abstract Title is required"),
    interestedInOther: Yup.string(),
  });

  const formik = useFormik({
    initialValues: {
      title: honorifics[0],
      name: "",
      email: "",
      organization: "",
      telephone: "",
      country: countries[0],
      interestedIn: interestedIns[0],
      abstractTitle: "",
      interestedInOther: "",
    },
    validationSchema,
    onSubmit: async (values, { resetForm }) => {
      const formData = new FormData();

      Object.entries(values).forEach(([key, value]: [key: string, value: any]) => {
        formData.append(key, value);
      });

      try {
        if (file) {
          const fileFormData = new FormData();
          fileFormData.append("file", file);
          const fileResponse = await fetch("/api/file", {
            method: "POST",
            body: fileFormData,
          });
          const fileResponseData = await fileResponse.json();
          formData.append("filePath", fileResponseData.filePath);
        }

        const response = await fetch("/api/abstract", {
          method: "POST",
          body: formData,
        });

        if (response.ok) {
          const responseData = await response.json();
          router.push("/");
          toast.success("Your abstract submitted");
          console.log("Response:", responseData);
        } else {
          console.error("Error:", response.statusText);
          toast.success("Sorry there are some errors");
        }
      } catch (error) {
        console.error("Error:", error);
        toast.success("Sorry there are some errors");
      }
    },
  });
  return (
    <main>
      <Breadcrumbs title="Submit abstract" items={[{ title: "Home", href: "/" }, { title: "Submit abstract" }]} />
      <div className="container my-10">
        <div className="grid md:grid-cols-12 grid-cols-1 gap-8">
          <div className="col-span-1" />
          <div className="col-span-9">
            <div className="grid md:grid-cols-3 grid-cols-1 gap-10">
              <div className="md:col-span-2 col-span-1">
                <div className="content">{<div dangerouslySetInnerHTML={{ __html: topics }} />}</div>
              </div>
              <div>
                <form className="grid grid-cols-1 gap-4" onSubmit={formik.handleSubmit}>
                  <div className=" col-span-1">
                    <span className="font-semibold">Sample abstract file:</span>{" "}
                    <Link
                      target="_blank"
                      download={"/Nomatech _compressed.pdf"}
                      className="text-btn-accent hover:underline"
                      href={"/Nomatech _compressed.pdf"}
                    >
                      Download
                    </Link>
                  </div>
                  <Select
                    label="Title"
                    items={honorifics}
                    name="title"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.title}
                    error={formik.touched.title ? formik.errors.title : undefined}
                  />
                  <Input
                    label="Name"
                    placeholder="Your name"
                    name="name"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    error={formik.touched.name ? formik.errors.name : undefined}
                  />
                  <Input
                    type="email"
                    label="Email"
                    placeholder="Your email"
                    name="email"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    error={formik.touched.email ? formik.errors.email : undefined}
                  />
                  <Input
                    label="Organization"
                    placeholder="Your organization"
                    name="organization"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    error={formik.touched.organization ? formik.errors.organization : undefined}
                  />
                  <Input
                    type="tel"
                    label="Telephone"
                    placeholder="Your telephone"
                    name="telephone"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    error={formik.touched.telephone ? formik.errors.telephone : undefined}
                  />
                  <Select
                    label="Country"
                    items={countries}
                    name="country"
                    value={formik.values.country}
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    error={formik.touched.country ? formik.errors.country : undefined}
                  />
                  <Select
                    label="Interested in"
                    items={interestedIns}
                    value={formik.values.interestedIn}
                    name="interestedIn"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    error={formik.touched.interestedIn ? formik.errors.interestedIn : undefined}
                  />

                  {formik.values.interestedIn === "Others" && (
                    <Input
                      label="Interested Other"
                      placeholder="Your Interested Other"
                      name="interestedInOther"
                      onBlur={formik.handleBlur}
                      onChange={formik.handleChange}
                      error={formik.touched.interestedInOther ? formik.errors.interestedInOther : undefined}
                    />
                  )}

                  <Input
                    label="Abstract Title"
                    placeholder="Your abstract title"
                    name="abstractTitle"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    error={formik.touched.abstractTitle ? formik.errors.abstractTitle : undefined}
                  />
                  <FileUpload onChange={setFile} />
                  <div className="col-span-1">
                    <Button type="submit" color="blue" className="w-full" disabled={!formik.isValid || !file || formik.isSubmitting}>
                      Send now
                    </Button>
                  </div>
                  <div className="  col-span-1">
                    <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="col-span-1" />
        </div>
      </div>
    </main>
  );
}
