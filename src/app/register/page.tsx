import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";

const content = `
<p><span style="font-weight: 400;">All persons attending the NomaTech ICMSN-2024 are asked to register by sending an e-mail including the following information to the e-mail address&nbsp;info@nomatech.mn before </span><strong>June 15, 2024</strong><span style="font-weight: 400;">:</span></p>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">Name&nbsp;&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Affiliation&nbsp;&nbsp;</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Contact e-mail address&nbsp;&nbsp;</span></li>
</ul>
<p><strong>CONFERENCE FEE</strong></p>
<table>
<tbody>
<tr>
<td>
<p><span style="font-weight: 400;">Category</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Fee</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Regular participants</span></p>
</td>
<td>
<p><span style="font-weight: 400;">250$</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Students/PhD students</span></p>
</td>
<td>
<p><span style="font-weight: 400;">50$</span></p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;">Participants registered as students need to confirm their status as full-time students by sending their student ID to the e-mail address: info@nomatech.mn</span></p>
<p><span style="font-weight: 400;">The registration fee covers the cost of conference materials, coffee breaks, lunches, and the conference dinner. It does not include accommodation or transportation.</span></p>
<p><strong>PAYMENT DETAILS</strong></p>
<table>
<tbody>
<tr>
<td>
<p><span style="font-weight: 400;">Account owner</span></p>
</td>
<td>
<p><span style="font-weight: 400;">National University of Mongolia</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Bank&rsquo;s name</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Khan Bank</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">SWIFT/BIC</span></p>
</td>
<td>
<p><span style="font-weight: 400;">AGMD MN UB</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Account number</span></p>
</td>
<td>
<p><span style="font-weight: 400;">5041769834</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">Transfer title</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Full name, nomatech icmsn 2024&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;">Please note: all bank charges are to be borne by the sender of the transfer.</span></p>`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Register" items={[{ title: "Home", href: "/" }, { title: "Register" }]} />
      <div className="container my-10">
        <div className="grid grid-cols-12 gap-8">
          <div className="md:col-span-2" />
          <div className="md:col-span-8 col-span-12">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
          </div>
          <div className="md:col-span-2" />
        </div>
      </div>
    </main>
  );
}
