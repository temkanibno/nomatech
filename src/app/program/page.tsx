import { Breadcrumbs } from "@nano/components/core/Breadcrumbs";

const content = `
<p><strong>DAY 1: </strong><span style="font-weight: 400;">08/07/2024</span></p>
<table>
<tbody>
<tr>
<td>
<p><span style="font-weight: 400;">08:00-09:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Registration and Welcome Breakfast</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">09:00-09:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Inaugural Address</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">09:30:10:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Plenary Talk&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">10:30-11:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Coffee break&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">11:00-13:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Session 1</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">13:00-14:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Lunch Break</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">14:00-16:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Session 2</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">16:00-16:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Tea break</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">16:30-17:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Panel Discussion: Bridging the Gap Between Research and Industry</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">17:30-21:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Airag reception/ Poster Sessions</span></p>
</td>
</tr>
</tbody>
</table>
<p><strong>DAY 2: </strong><span style="font-weight: 400;">09/07/2024</span></p>
<table>
<tbody>
<tr>
<td>
<p><span style="font-weight: 400;">08:00-09:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Breakfast and Networking</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">09:00-10:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Plenary Talk&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">10:00-10:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Coffee break /Posters/Exhibition</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">10:30-12:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Session 3</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">12:30-13:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Lunch Break</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">13:30-14:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Workshop: Research Ethics and Safety&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">14:30-16:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Special Session</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">16:30-17:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Panel Discussion: International Collaborations in Materials Research&nbsp;</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">17:30-17:30</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Closing Remarks and Acknowledgements</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="font-weight: 400;">17:30-21:00</span></p>
</td>
<td>
<p><span style="font-weight: 400;">Evening Banquet/ Art &amp; Science Events</span></p>
</td>
</tr>
</tbody>
</table>`;

export default function About() {
  return (
    <main>
      <Breadcrumbs title="Program" items={[{ title: "Home", href: "/" }, { title: "Program" }]} />
      <div className="container md:my-10 my-5">
        <div className="grid md:grid-cols-12 gap-8">
          <div className="col-span-2" />
          <div className="col-span-8">
            <div className="content">{<div dangerouslySetInnerHTML={{ __html: content }} />}</div>
          </div>
          <div className="col-span-2" />
        </div>
      </div>
    </main>
  );
}
