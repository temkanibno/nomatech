import { NextRequest, NextResponse } from "next/server";

import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3";
import { nanoid } from "nanoid";

const Bucket = process.env.AWS_S3_BUCKET;
const s3 = new S3Client({
  region: process.env.AWS_REGION!,
  credentials: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID!,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY!,
  },
});

const MAX_SIZE = 10 * 1024 ** 2;

export async function POST(request: NextRequest) {
  const formData = await request.formData();
  const files = formData.getAll("file") as File[];
  const file = files[0];

  const ext = file.name.split(".").pop();

  if (!file) return NextResponse.json({ message: "File not found" }, { status: 400 });
  if (file.size > MAX_SIZE) return NextResponse.json({ message: "File too big" }, { status: 400 });
  const Body = (await file.arrayBuffer()) as Buffer;
  const Key = `${nanoid()}.${ext}`;
  const response = await s3.send(new PutObjectCommand({ Bucket, Key, Body }));
  if (response.$metadata.httpStatusCode !== 200) NextResponse.json({ message: "File upload failed" }, { status: 500 });
  return NextResponse.json({ filePath: `https://${Bucket}.s3.${process.env.AWS_REGION!}.amazonaws.com/${Key}` });
}
