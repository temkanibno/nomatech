import { NextRequest, NextResponse } from "next/server";

import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "nomatech23@gmail.com",
    pass: "lltb fmlj iwjs huan",
  },
});

export const POST = async (req: NextRequest) => {
  const formData = await req.formData();
  const abstractTitle = formData.get("abstractTitle");
  const title = formData.get("title");
  const country = formData.get("country");
  const email = formData.get("email");
  const interestedIn = formData.get("interestedIn");
  const interestedInOther = formData.get("interestedInOther");
  const name = formData.get("name");
  const organization = formData.get("organization");
  const telephone = formData.get("telephone");
  const filePath = formData.get("filePath");
  try {
    const emailTemplate = `
      <html>
        <head>
          <style>
            body {
              font-family: Arial, sans-serif;
            }
          </style>
        </head>
        <body>
          <p>Hello ${title} ${name} just submitted abstract</p>

          <p>Abstract details:</p>
          
          <ul>
            <li>Abstract Title: ${abstractTitle}</li>
            <li>Country: ${country}</li>
            <li>Email: ${email}</li>
            <li>Interested In: ${interestedIn === "Others" ? interestedInOther : interestedIn}</li>
            <li>Organization: ${organization}</li>
            <li>Telephone: ${telephone}</li>
            <li>Abstract attachment: <a href="${filePath}" target="_blank">Download attachment</a></li>
          </ul>
        </body>
      </html>
    `;

    const mailOptions: Mail.Options = {
      from: "nomatech23@gmail.com",
      to: "info@nomatech.mn",
      subject: `Abstract Submission: "${abstractTitle}" by ${title}${name}`,
      html: emailTemplate,
    };
    await transporter.sendMail(mailOptions);
    return NextResponse.json("Abstract send success", { status: 200 });
  } catch (error) {
    console.error("Error:", error);
    return NextResponse.json("Internal Server Error", { status: 500 });
  }
};
