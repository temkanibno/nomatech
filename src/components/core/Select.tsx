import { nanoid } from "nanoid";

type SelectProps = {
  label?: string;
  items: string[];
  onChange?: (e: React.ChangeEvent<HTMLSelectElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLSelectElement>) => void;
  name?: string;
  error?: string;
  value?: string;
};
export const Select: React.FC<SelectProps> = (props) => {
  const { label, items, onChange, onBlur, name, error, value } = props;
  return (
    <label className="form-control w-full">
      <div className="label">
        <span className="label-text font-medium text-base">{label}</span>
      </div>
      <select className="select select-bordered" onChange={onChange} onBlur={onBlur} name={name} value={value}>
        {items.map((item) => (
          <option value={item} key={nanoid()}>
            {item}
          </option>
        ))}
      </select>
      {error && <span className="text-error text-sm mt-2">{error}</span>}
    </label>
  );
};
