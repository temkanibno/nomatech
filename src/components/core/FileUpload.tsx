import React, { useEffect, useMemo } from "react";
import { useDropzone } from "react-dropzone";
import { toast } from "react-toastify";

const humanFileSize = (bytes: number, si = false, dp = 1) => {
  const thresh = si ? 1000 : 1024;

  if (Math.abs(bytes) < thresh) {
    return bytes + " B";
  }

  const units = si ? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"] : ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
  let u = -1;
  const r = 10 ** dp;

  do {
    bytes /= thresh;
    ++u;
  } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);

  return bytes.toFixed(dp) + " " + units[u];
};
const baseStyle = {
  cursor: "pointer",
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const focusedStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

export const FileUpload = ({ onChange }: { onChange: (file: File | null) => void }) => {
  const { acceptedFiles, getRootProps, getInputProps, isFocused, isDragAccept, isDragReject } = useDropzone({
    maxFiles: 1,
    multiple: false,
    maxSize: 10 * 1024 * 1024,
    onDropRejected: () => {
      toast.error("File can't be over 10MB");
    },
  });

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isFocused ? focusedStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isFocused, isDragAccept, isDragReject]
  );

  useEffect(() => {
    onChange(acceptedFiles[0] || null);
  }, [acceptedFiles, onChange]);

  const files = acceptedFiles.map((file) => (
    <li key={file.name}>
      {file.name} - {humanFileSize(file.size)}
    </li>
  ));

  return (
    // @ts-ignore
    <div {...getRootProps()} style={style}>
      <input {...getInputProps()} />
      {acceptedFiles.length > 0 ? <ul>{files}</ul> : <p>Drop some files here, or click to select files</p>}
    </div>
  );
};
