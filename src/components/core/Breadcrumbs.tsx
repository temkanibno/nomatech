import { ChevronRight } from "@nano/icons";
import { nanoid } from "nanoid";
import Image from "next/image";
import Link from "next/link";

type BreadcrumbItem = {
  title: string;
  href?: string;
};

type BreadcrumbsProps = {
  title: string;
  items: BreadcrumbItem[];
};

export const Breadcrumbs: React.FC<BreadcrumbsProps> = (props) => {
  const { title, items } = props;

  const [firstTitle, ...restTitle] = title.split(" ");

  return (
    <div className="md:py-24 py-12 relative">
      <Image
        src={"/images/image8.jpg"}
        width={1200}
        height={600}
        alt={title}
        className="absolute left-0 top-0 w-full h-full object-cover object-bottom"
      />
      <div className="container relative">
        <div className="inline-flex flex-col md:p-12 p-6 md:w-auto w-full bg-btn-accent/90">
          <h1 className="md:text-5xl text-2xl font-bold text-white">
            <span className="text-black">{firstTitle}</span> {restTitle.join(" ")}
          </h1>
          <ol className="flex flex-wrap gap-4 pt-4 mt-4 border-t border-[#fff]">
            {items.map((item, index) => {
              if (index === items.length - 1)
                return (
                  <li key={nanoid()}>
                    <span className="text-white font-medium text-lg">{item.title}</span>
                  </li>
                );
              return (
                <li key={nanoid()} className="flex items-center">
                  <Link className="text-black font-medium text-lg" href={`${item.href}`}>
                    {item.title}
                  </Link>
                  <span className="select-none ml-4 text-white inline-flex">
                    <ChevronRight />
                  </span>
                </li>
              );
            })}
          </ol>
        </div>
      </div>
    </div>
  );
};
