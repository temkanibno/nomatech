import { ReactNode } from "react";
import { twMerge } from "tailwind-merge";

type ButtonProps = {
  children: ReactNode;
  onClick?: () => void;
  href?: string;
  className?: string;
  color?: "white" | "blue" | "black";
  as?: React.ElementType;
  type?: "button" | "submit";
  download?: string;
  target?: string;
  disabled?: boolean;
  loading?: boolean;
};

export const Button = (props: ButtonProps) => {
  const { children, href, onClick, type = "button", color = "white", className, as = "button", download, target, disabled, loading } = props;
  const Component: React.ElementType = as;
  const baseStyle = `btn font-medium text-sm text-black hover:text-white rounded-none md:px-6 md:py-4 px-3 py-2 inline-flex shadow-none border-0 relative overflow-hidden bg-white hover:bg-white`;

  // Define styles for disabled state
  const disabledStyle = disabled ? "opacity-50 cursor-not-allowed" : "";

  // Define styles for loading state
  const loadingStyle = loading ? "pointer-events-none opacity-50" : "";

  return (
    <Component
      disabled={disabled || loading} // Disable button if either disabled or loading
      type={type}
      href={href}
      onClick={onClick}
      className={`${twMerge(baseStyle, className)} primary-btn ${color} ${disabledStyle} ${loadingStyle}`}
      target={target}
      download={download}
    >
      {loading ? <span className={`inline-flex relative pr-10`}>Loading...</span> : <span className={`inline-flex relative pr-10`}>{children}</span>}
    </Component>
  );
};
