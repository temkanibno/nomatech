import { HTMLInputTypeAttribute } from "react";

type InputProps = {
  label?: string;
  placeholder?: string;
  type?: HTMLInputTypeAttribute;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  name?: string;
  error?: string;
};
export const Input: React.FC<InputProps> = (props) => {
  const { label, placeholder, onChange, onBlur, name, error, type = "text" } = props;
  return (
    <label className="form-control w-full">
      <div className="label">
        <span className="label-text font-medium text-base">{label}</span>
      </div>
      {type === "file" ? (
        <input type="file" name={name} onChange={onChange} onBlur={onBlur} className="file-input file-input-bordered w-full" />
      ) : (
        <input type={type} name={name} onBlur={onBlur} onChange={onChange} placeholder={placeholder} className="input input-bordered" />
      )}
      {error && <span className="text-error text-sm mt-2">{error}</span>}
    </label>
  );
};
