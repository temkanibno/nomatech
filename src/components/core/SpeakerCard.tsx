import { Globe, Google, ResearchGate } from "@nano/icons";
import { nanoid } from "nanoid";
import Image from "next/image";
import Link from "next/link";

export const SpeakerCard = ({ speaker, plenary, small }: any) => {
  const iconClasses = small ? "w-6 h-6 text-xl" : "w-10 h-10 text-2xl";
  return (
    <div key={nanoid()}>
      <div className="border-4 border-solid border-transparent drop-shadow-sm hover:drop-shadow-2xl transition-all duration-300 group">
        <div className="aspect-square relative">
          <Image
            width={210}
            height={210}
            src={speaker.image}
            alt={speaker.name}
            className="absolute top-0 left-0 w-full h-full object-cover object-top grayscale group-hover:grayscale-0 duration-300 transition-all"
          />
          <div className="absolute bottom-0 right-0 p-4 flex gap-3 opacity-0 group-hover:opacity-100 duration-500 transition-all">
            {speaker.website && (
              <Link
                target="_blank"
                href={speaker.website}
                className={`${iconClasses} bg-btn-accent/50 hover:bg-btn-accent text-white grid place-items-center  duration-300 transition-all`}
              >
                <Globe />
              </Link>
            )}
            {speaker.researchGate && (
              <Link
                target="_blank"
                href={speaker.researchGate}
                className={`${iconClasses} bg-btn-accent/50 hover:bg-btn-accent text-white grid place-items-center duration-300 transition-all`}
              >
                <ResearchGate />
              </Link>
            )}
            {speaker.googleScholar && (
              <Link
                target="_blank"
                href={speaker.googleScholar}
                className={`${iconClasses} bg-btn-accent/50 hover:bg-btn-accent text-white grid place-items-center  duration-300 transition-all`}
              >
                <Google />
              </Link>
            )}
          </div>
        </div>
        <div className="p-4 bg-white/80 block">
          {plenary && <p className="text-[#363636] italic text-center line-clamp-1 h-6 overflow-hidden">Plenary speaker</p>}
          <h5 className="font-bold text-[#000] text-center line-clamp-1 h-7 overflow-hidden">{speaker.name}</h5>
          <p className="text-[#000] text-center overflow-hidden">{speaker.affiliation}</p>
        </div>
      </div>
    </div>
  );
};
