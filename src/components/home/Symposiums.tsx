import { CheckCircle } from "@nano/icons";
import { nanoid } from "nanoid";
import { Button } from "../core/Button";
import Link from "next/link";

const topics = [
  { title: "Session 1", description: "NEXT-GENERATION ADVANCED MATERIALS: FUNDAMENTALS & APPLICATIONS" },
  { title: "Session 2", description: "EMERGING FUNCTIONAL MATERIALS & DEVICES" },
  { title: "Session 3", description: "NOVEL TRENDS IN NANOSTRUCTURES & NANOMATERIALS: DESIGN, CHARACTERISTICS, & APPLICATIONS" },
  { title: "Session 4", description: "CURRENT AND FUTURE PROSPECTS OF BIO & NANOTECHNOLOGY" },
  { title: undefined, description: "Poster Sessions And Art & Science Events" },
];

export const Symposiums = () => {
  return (
    <div className="container -mt-10">
      <section className="flex flex-col items-center gap-4 lg:p-20 md:p-10 p-5">
        <h2 className="font-semibold md:text-4xl text-2xl text-btn-accent md:mb-8 mb-4">Sessions</h2>
        <ul className="grid grid-cols-1 gap-4">
          {topics.map((topic) => (
            <li key={nanoid()} className="flex items-center gap-4 group cursor-pointer [&>*]:duration-300">
              <span className="group-hover:text-btn-accent text-4xl flex">
                <CheckCircle />
              </span>
              <div className="group-hover:text-btn-accent">
                {topic.title && <span className="font-bold mr-2">{topic.title}:</span>}
                <span>{topic.description}</span>
              </div>
            </li>
          ))}
        </ul>
        <div className="flex gap-4 my-4 md:flex-row flex-col">
          <Button color="blue" as={Link} href="/submit-abstract">
            Sessions
          </Button>
          <Button as={Link} download={"/Nomatech _compressed.pdf"} target="_blank" href={"/Nomatech _compressed.pdf"} color="black">
            Download Sample
          </Button>
        </div>
      </section>
    </div>
  );
};
