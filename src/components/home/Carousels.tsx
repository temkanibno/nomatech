"use client";
import Image from "next/image";
import { nanoid } from "nanoid";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { CarouselCountdown } from "./CarouselCountdown";

const carousel = {
  image: "/images/image8.jpg",
  title: "NomaTech Mongolia 2024",
  description: "International Conference on Materials Science & Nanotechnology",
  starts: "JULY 8-9, 2024 | ACADEMIC SENATE HALL, NATIONAL UNIVERSITY OF MONGOLIA",
  startDate: new Date("2024-07-08"),
};

export const Carousels = () => {
  return (
    <div className="md:h-[680px] h-auto bg-[#ccc]" key={nanoid()}>
      <div className="relative h-full overflow-hidden">
        <Image
          src={carousel.image}
          width={1920}
          height={680}
          alt={carousel.title}
          className="absolute top-0 left-0 w-full h-full object-cover object-bottom"
        />
        <div className=" w-full h-full bg-btn-accent/90 absolute"></div>
        <div className="container relative h-full">
          <div className="grid md:grid-cols-2 grid-cols-1 h-full">
            <div className="carousel col-span-1 h-full flex flex-col justify-center gap-5 items-start pr-10 md:py-0 py-6">
              <h2 className="carousel-title md:text-6xl text-2xl line-clamp-2 font-bold text-white ">{carousel.title}</h2>
              <p className="carousel-desc font-medium line-clamp-4 text-white md:text-lg text-base">{carousel.description}</p>
              <p className="carousel-desc font-medium line-clamp-4 text-white md:text-lg text-base">{carousel.starts}</p>
              {/* <CarouselCountdown date={carousel.startDate} /> */}
            </div>
            <div className="flex flex-col w-full h-full justify-center md:ml-6 md:py-0 py-6">
              <div className="text-white text-center mb-6 font-bold md:text-3xl text-xl animate-bounce opacity-80">Conference Highlights</div>
              <div className="relative aspect-video w-full">
                <iframe
                  className="absolute inset-0 w-full h-full"
                  width="1920"
                  height="1080"
                  src="https://www.youtube.com/embed/e30rrx4w7lA?si=PjhOJtvqZZY4gaBD"
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  referrerPolicy="strict-origin-when-cross-origin"
                  allowFullScreen
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
