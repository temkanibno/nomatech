"use client";

import { useEffect, useState } from "react";

export const CarouselCountdown = ({ date }: { date: Date }) => {
  const [remaining, setRemaining] = useState({ days: 0, hours: 0, minutes: 0, seconds: 0 });

  const countdown = (date: Date) => {
    const now = new Date().getTime();
    const distance = date.getTime() - now;
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);
    return { days, hours, minutes, seconds };
  };

  useEffect(() => {
    setInterval(() => {
      setRemaining(countdown(date));
    }, 1000);
  }, [date]);

  const { days, hours, minutes, seconds } = remaining;

  return (
    <div className="flex gap-2 items-center">
      <div className="text-4xl text-white font-bold mr-10">See You In:</div>
      <div className="flex flex-col items-center">
        <span className="text-4xl font-bold text-white">{days}</span>
        <span className="text-sm font-medium text-white">Days</span>
      </div>
      <div className="flex flex-col items-center">
        <span className="text-4xl font-bold text-white">{hours}</span>
        <span className="text-sm font-medium text-white">Hours</span>
      </div>
      <div className="flex flex-col items-center">
        <span className="text-4xl font-bold text-white">{minutes}</span>
        <span className="text-sm font-medium text-white">Minutes</span>
      </div>
      <div className="flex flex-col items-center">
        <span className="text-4xl font-bold text-white">{seconds}</span>
        <span className="text-sm font-medium text-white">Seconds</span>
      </div>
    </div>
  );
};
