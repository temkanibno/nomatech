"use client";
import { nanoid } from "nanoid";
import Image from "next/image";
import Link from "next/link";
import Slider, { Settings } from "react-slick";

const participants = [
  {
    image: "/participants/0.webp",
    title: "tokyo institute of technology",
    href: "https://www.titech.ac.jp/english",
  },
  {
    image: "/participants/1.webp",
    title: "university of cambridge",
    href: "https://www.cam.ac.uk/",
  },
  {
    image: "/participants/2.webp",
    title: "yokohama national university",
    href: "https://www.ynu.ac.jp/english/",
  },
  {
    image: "/participants/3.webp",
    title: "technical university of denmark",
    href: "https://www.dtu.dk/english/",
  },
  {
    image: "/participants/4.webp",
    title: "griffith university",
    href: "https://www.griffith.edu.au/",
  },
  {
    image: "/participants/5.webp",
    title: "Seoul National University",
    href: "https://en.snu.ac.kr/",
  },
  {
    image: "/participants/6.webp",
    title: "inchean national university",
    href: "https://www.inu.ac.kr/inu/index.do?epTicket=LOG",
  },
  {
    image: "/participants/9.webp",
    title: "The University of Queensland",
    href: "https://www.uq.edu.au",
  },
  {
    image: "/participants/10.webp",
    title: "BioMed X Heidelberg",
    href: "https://bio.mx",
  },
  {
    image: "/participants/11.webp",
    title: "The University of Adelaide",
    href: "https://www.adelaide.edu.au",
  },
  {
    image: "/participants/12.webp",
    title: "Changwon National University",
    href: "https://www.changwon.ac.kr",
  },
  {
    image: "/participants/13.webp",
    title: "Tohoku University",
    href: "https://www.tohoku.ac.jp",
  },
  {
    image: "/participants/14.webp",
    title: "Kyushu University",
    href: "https://www.kyushu-u.ac.jp",
  },
  {
    image: "/participants/15.webp",
    title: "Hanyang University",
    href: "https://www.hanyang.ac.kr",
  },
  {
    image: "/participants/16.webp",
    title: "Inner Mongolia University",
    href: "https://www.imu.edu.cn",
  },
  {
    image: "/participants/17.webp",
    title: "Dalian University",
    href: "https://www.dlu.edu.cn",
  },
  {
    image: "/participants/18.webp",
    title: "QNS Center",
    href: "https://qns.science",
  },
];

const settings: Settings = {
  infinite: true,
  slidesToShow: 9,
  slidesToScroll: 1,
  autoplay: true,
  speed: 1000,
  autoplaySpeed: 1000,
  arrows: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 9,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ],
};

export const Participants = () => {
  return (
    <section className="my-20">
      <div className="container">
        <h2 className="text-center font-semibold text-4xl text-btn-accent mb-4">Participants from </h2>
        <p className="text-center text-lg mb-8">Who Believed In Us</p>
        <Slider {...settings}>
          {participants.map((participant, index) => (
            <Link href={participant.href} target="_blank" key={nanoid()}>
              <Image width={374} height={187} src={participant.image} alt={participant.title} />
            </Link>
          ))}
        </Slider>
      </div>
    </section>
  );
};
