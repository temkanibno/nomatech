"use client";
import { nanoid } from "nanoid";
import Image from "next/image";
import Slider, { Settings } from "react-slick";

const sponsors = [
  {
    image: "/sponsors/golden/golden_erdenet.webp",
  },
  {
    image: "/sponsors/golden/golden_ivc.webp",
  },
  {
    image: "/sponsors/golden/shutsan.webp",
  },
  {
    image: "/sponsors/golden/golden_suu.webp",
  },
];

const settings: Settings = {
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,
  responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ],
};

export const Golden = () => {
  return (
    <section className="my-20">
      <div className="container">
        <h2 className="text-center font-semibold text-4xl text-btn-accent mb-4">Golden Sponsors</h2>
        <p className="text-center text-lg mb-8">NomaTech ICMSN-2024</p>
        <Slider {...settings}>
          {sponsors.map((participant) => (
            <Image width={374} height={187} src={participant.image} alt={""} key={nanoid()} />
          ))}
        </Slider>
      </div>
    </section>
  );
};
