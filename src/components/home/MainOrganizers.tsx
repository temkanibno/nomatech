"use client";
import { nanoid } from "nanoid";
import Image from "next/image";
import Slider, { Settings } from "react-slick";

const mainOrganizers = [
  {
    image: "/sponsors/main/main_bolovsrol.webp",
    title: "Ministry of education and science",
  },
  {
    image: "/sponsors/main/main_num.webp",
    title: "national university of mongolia",
  },
  {
    image: "/sponsors/main/main_erdem.webp",
    title: "mongolian academy of science",
  },
  {
    image: "/sponsors/main/main_nano.webp",
    title: "nanocenter",
  },
];

const settings: Settings = {
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,
  responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ],
};

export const MainOrganizers = () => {
  return (
    <section className="my-20">
      <div className="container">
        <h2 className="text-center font-semibold text-4xl text-btn-accent mb-4">Main Organizers</h2>
        <p className="text-center text-lg mb-8">NomaTech ICMSN-2024</p>
        <Slider {...settings}>
          {mainOrganizers.map((participant) => (
            <Image key={nanoid()} width={374} height={187} src={participant.image} alt={participant.title} />
          ))}
        </Slider>
      </div>
    </section>
  );
};
