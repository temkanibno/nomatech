"use client";
import { nanoid } from "nanoid";
import Image from "next/image";
import Slider, { Settings } from "react-slick";

const sponsors = [
  {
    image: "/sponsors/supporting/businesscounsil.webp",
  },
  {
    image: "/sponsors/supporting/scafe.webp",
  },
  {
    image: "/sponsors/supporting/tengertv.webp",
  },
];

const settings: Settings = {
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: false,
  responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ],
};

export const Supporting = () => {
  return (
    <section className="my-20">
      <div className="container">
        <h2 className="text-center font-semibold text-4xl text-btn-accent mb-4">Supporting organizations</h2>
        <p className="text-center text-lg mb-8">NomaTech ICMSN-2024</p>
        <Slider {...settings}>
          {sponsors.map((sponsor) => (
            <Image width={374} height={187} src={sponsor.image} alt={""} key={nanoid()} />
          ))}
        </Slider>
      </div>
    </section>
  );
};
