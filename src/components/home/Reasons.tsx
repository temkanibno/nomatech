import { Flask, Comments, LightBulb, NetworkWired, Globe, HandShake } from "@nano/icons";
import { nanoid } from "nanoid";
import Image from "next/image";

const reasons = [
  {
    icon: <Flask />,
    title: "Cutting-Edge Knowledge",
    description:
      "Step into the forefront of interdisciplinary research within materials science and nanotechnology, exploring the latest advancements and breakthroughs across diverse fields.",
  },
  {
    icon: <Comments />,
    title: "Interactive Sessions",
    description:
      "Immerse yourself in dynamic sessions, including plenary talks, oral and poster presentations, and exhibitions, fostering cross-disciplinary discussions and knowledge exchange.",
  },
  {
    icon: <LightBulb />,
    title: "Foundation Insights",
    description: "Gain valuable insights from international and national foundations, showcasing opportunities and addressing challenges.",
  },
  {
    icon: <NetworkWired />,
    title: "Collaborative Nexus",
    description: "Explore avenues for collaboration between educational institutions, industries, and research organizations in Mongolia.",
  },
  {
    icon: <Globe />,
    title: "Unique Cultural Experience",
    description:
      "Experience the charm of Mongolia and its nomadic traditions, adding a cultural dimension to your interdisciplinary exploration and networking.",
  },
  {
    icon: <HandShake />,
    title: "Community Engagement",
    description:
      "NomaTech ICMSN-2024 is committed to community engagement, providing opportunities for researchers, students, and professionals to connect, share insights, and build lasting relationships within the scientific community.",
  },
];

export const Reasons = () => {
  return (
    <section>
      <div className="container">
        <div className="lg:p-20 md:p-10 p-5 md:pb-28 pb-14 bg-[#0a1426] text-white flex flex-col items-center gap-4 relative">
          <Image src={"/images/hex.png"} width={612} height={408} alt="why attend?" className="absolute object-cover left-0 top-0 w-full h-full" />
          <h2 className="text-[#fff] md:text-5xl text-2xl font-bold">Why Attend?</h2>
          <p className="md:text-lg text-base font-medium">NomaTech ICMSN-2024</p>
        </div>
        <div className="-mt-8 md:px-8 px-4 grid xl:grid-cols-3 lg:grid-cols-2 relative gap-3">
          {reasons.map((reason) => (
            <div
              key={nanoid()}
              className="bg-white md:px-8 md:py-10 px-4 py-5 flex md:flex-row flex-col md:items-start items-center gap-4 shadow-sm hover:shadow-lg group duration-300"
            >
              <div className="text-[#0a1426] group-hover:text-[#2363d5] duration-300 text-6xl">{reason.icon}</div>
              <div className="flex flex-col gap-4">
                <h3 className="text-xl font-bold md:text-left text-center group-hover:text-[#2363d5] duration-300">{reason.title}</h3>
                <p className="text-lg md:text-left text-center">{reason.description}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};
