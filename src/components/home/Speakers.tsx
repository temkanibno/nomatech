"use client";
import { mainSpeakers } from "@nano/utils/speakers";
import { nanoid } from "nanoid";
import Slider, { Settings } from "react-slick";
import { SpeakerCard } from "../core/SpeakerCard";
import Link from "next/link";
import { Button } from "../core/Button";

const settings: Settings = {
  infinite: false,
  slidesToShow: 5,
  slidesToScroll: 1,
  autoplay: true,
  speed: 1000,
  autoplaySpeed: 3000,
  arrows: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ],
};

export const Speakers = () => {
  return (
    <section className="my-20">
      <div className="container">
        <Link href={"/speakers"} className="text-center font-semibold text-4xl text-btn-accent mb-4 block">
          Meet
        </Link>
        <Link href={"/speakers"} className="text-center text-lg mb-8 block">
          Plenary speakers
        </Link>
        <Slider {...settings}>
          {[...mainSpeakers].map((speaker) => (
            <div className="md:p-4 sm:p-2" key={nanoid()}>
              <SpeakerCard speaker={speaker} />
            </div>
          ))}
        </Slider>
      </div>
      <div className="flex justify-center my-5">
        <Button as={Link} color="blue" href="/speakers">
          More speakers
        </Button>
      </div>
    </section>
  );
};
