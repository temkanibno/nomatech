import Image from "next/image";
import { Button } from "../core/Button";
import Link from "next/link";

export const AboutUs = () => {
  return (
    <section className="lg:py-20 md:py-10 py-5 mb-5">
      <div className="container">
        <div className="grid lg:grid-cols-2 grid-cols-1 gap-6">
          <div>
            <div className="relative pr-20 pb-20">
              <Image src="/images/item.jpeg" alt="image" width={300} height={300} className="p-3 bg-white shadow-md w-full" />
              <Image src="/images/mat.png" alt="image" width={300} height={300} className="p-3 bg-white shadow-md absolute bottom-0 right-0 w-1/2" />
            </div>
          </div>
          <div className="flex flex-col gap-4 items-start">
            <h2 className="text-[#1f2632] md:text-5xl text-3xl md:text-left text-center font-bold">NomaTech ICMSN-2024</h2>
            <p className="text-base md:text-lg text-justify">
              The core objectives of the <strong>NomaTech ICMSN-2024</strong> are to delve deep into the most recent innovations, research, and
              applications in both material science and nanotechnology, showcasing the forefront of global scientific advancements and also playing a
              pivotal role in amplifying local collaborations within Mongolia. By providing a platform where Mongolian researchers, institutions, and
              industry experts could interact directly with international counterparts, the conference has fostered an environment conducive to
              collaborative partnerships.
            </p>
            <p className="text-base md:text-lg text-justify">
              We are thrilled to extend a cordial invitation to researchers, experts, and innovators from across the globe to join us for the
              inaugural International Conference on Materials Science and Nanotechnology. This three-day scientific gathering is set to take place in
              the heart of <i>Ulaanbaatar, Mongolia, from July 8 to 9, 2024.</i>
            </p>
            <p className="text-base md:text-lg text-justify">
              <b className="text-bold mb-2">Who Should Attend</b>
              <br />
              The conference is open to all the researchers who are interested in exploring the research in material science & nanotechnology. For
              further information, please contact conference organizer{" "}
              <Link href="https://www.linkedin.com/in/md69/" className="hover:underline hover:text-btn-accent" target="_blank">
                Maral Davaanyam
              </Link>
              -{" "}
              <Link href="mailto:info@nomatech.mn" className="hover:underline hover:text-btn-accent">
                info@nomatech.mn
              </Link>
            </p>
            <div className="flex w-full md:justify-start justify-center">
              <Button color="blue" as={Link} href={"/about"}>
                Read more
              </Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
