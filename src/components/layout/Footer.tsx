import { Envelope, Facebook, LinkedIn, MapMarker, PhoneIcon } from "@nano/icons";
import { nanoid } from "nanoid";
import Image from "next/image";
import Link from "next/link";

const contacts = [
  { icon: <PhoneIcon />, link: "tel:+97685050500", text: "+976 85050500" },
  {
    icon: <MapMarker />,
    link: "https://maps.app.goo.gl/mtShXuLmNqay8jg76",
    text: "Ikh surguuliin gudamj-1 p.o.box -46a/523, 14201 Ulaanbaatar, Mongolia",
  },
  { icon: <Envelope />, link: "mailto:info@nomatech.mn", text: "info@nomatech.mn" },
];

const socials = [
  { icon: <Facebook />, link: "https://www.facebook.com/nanocentermn" },
  { icon: <LinkedIn />, link: "https://www.linkedin.com/company/nanocenter-mn/" },
];

const menus = [
  {
    title: "Quick links",
    items: [
      { title: "Speakers", link: "/speakers" },
      { title: "Abstract Submission", link: "/submit-abstract" },
      { title: "Register", link: "/register" },
      { title: "Venue", link: "/venue" },
    ],
  },
  {
    title: "NomaTech ICMSN",
    items: [
      { title: "About", link: "/about" },
      { title: "Privacy Policy", link: "/privacy-policy" },
      { title: "Diversity Statement", link: "/diversity-statement" },
    ],
  },
];

export const Footer = () => {
  return (
    <footer className="flex flex-col gap-8 bg-btn-accent relative">
      <div className="grid 2xl:grid-cols-4">
        <div className="md:p-16 p-8">
          <div className="flex flex-col 2xl:items-start items-center gap-4">
            <Link href="/">
              <Image src={"/logo.png"} width={75} height={75} alt="NomaTech" className="hover:grayscale-1" />
            </Link>
            <p className="font-lg text-white 2xl:text-justify text-center">
              The core objectives of the NomaTech ICMSN-2024” are to delve deep into the most recent innovations, research, and applications in both
              material science and nanotechnology, showcasing the forefront of global scientific advancements and also playing a pivotal role in
              amplifying local collaborations within Mongolia.
            </p>
            <nav>
              <ul className="flex gap-4">
                {socials.map((social) => (
                  <li key={nanoid()}>
                    <Link
                      href={social.link}
                      target="_blank"
                      className="size-10 grid place-content-center text-white shadow-md hover:bg-white hover:text-btn-accent duration-300 text-lg"
                    >
                      {social.icon}
                    </Link>
                  </li>
                ))}
              </ul>
            </nav>
          </div>
        </div>
        <div className="col-span-3">
          <div className="md:p-16 p-8 bg-[#F2F6FD]">
            <div className="grid gap-4 md:grid-cols-3 grid-cols-1">
              {menus.map((menu) => (
                <div key={nanoid()} className="md:text-left text-center">
                  <h4 className="text-[#1f2632] text-2xl font-semibold mb-4">{menu.title}</h4>
                  <ul className="flex flex-col md:items-start items-center gap-2">
                    {menu.items.map((item) => (
                      <li key={nanoid()}>
                        <Link href={item.link} className="capitalize text-[#454545] pl-0 hover:pl-2 hover:text-btn-accent duration-300">
                          {item.title}
                        </Link>
                      </li>
                    ))}
                  </ul>
                </div>
              ))}
              <div className="md:text-left text-center">
                <h4 className="text-[#1f2632] text-2xl font-semibold mb-4">Get In Touch</h4>
                <ul className="flex flex-col md:items-start items-center gap-2">
                  {contacts.map((contact) => (
                    <li key={nanoid()}>
                      <Link
                        href={contact.link}
                        className="text-[#454545] pl-0 hover:pl-2 hover:text-btn-accent duration-300 flex items-center gap-4 md:flex-row flex-col"
                      >
                        <span className="flex-shrink-0 size-10 grid place-items-center bg-btn-accent text-white text-2xl">{contact.icon}</span>
                        {contact.text}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
          <p className="text-center text-white py-8">Copyright © {new Date().getFullYear()} All rights reserved | NomaTech ICMSN</p>
        </div>
      </div>
    </footer>
  );
};
