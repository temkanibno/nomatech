"use client";

import { Bars } from "@nano/icons";
import { Button } from "../core/Button";
import { useEffect, useRef, useState } from "react";
import { menus } from "@nano/utils/menus";
import Link from "next/link";
import { nanoid } from "nanoid";

export const MobileNav = () => {
  const dialogRef = useRef<null | HTMLDialogElement>(null);
  const [onClick, setOnClick] = useState(() => {});
  useEffect(() => {
    if (dialogRef.current) {
      setOnClick(() => () => {
        dialogRef.current?.showModal();
      });
    }
  }, [dialogRef]);

  const flattenedMenus: any[] = [];

  menus.forEach((menu) => {
    flattenedMenus.push(menu);
    if (menu.children) {
      menu.children.forEach((child) => {
        flattenedMenus.push(child);
      });
    }
  });

  return (
    <>
      <div className="block md:hidden">
        {/* @ts-ignore */}
        <Button color="black" className="[&>span]:after:hidden [&>span]:pr-0 px-4 [&>span]:text-lg" onClick={onClick}>
          <Bars />
        </Button>
      </div>
      <dialog ref={dialogRef} className="modal">
        <div className="modal-box bg-transparent backdrop-blur-md">
          <nav>
            <ul className="flex flex-col items-center gap-4">
              {flattenedMenus.map((menu) => (
                <li key={nanoid()} className="text-center">
                  {menu.name === "Brochure" ? (
                    <Link className="text-lg font-medium text-white text-center" href={"/brochure.pdf"} download={"/brochure.pdf"} target="_blank">
                      {menu.name}
                    </Link>
                  ) : (
                    <Link className="text-lg font-medium text-white text-center" href={menu.link}>
                      {menu.name}
                    </Link>
                  )}
                </li>
              ))}

              <li>
                <Button href="/submit-abstract" as={Link} color="black">
                  Register
                </Button>
              </li>
              <li>
                <Button href="/submit-abstract" as={Link} color="blue">
                  Submit Abstract
                </Button>
              </li>
            </ul>
          </nav>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
    </>
  );
};
