import { BusinessTime, MapMarker, PhoneIcon } from "@nano/icons";
import Link from "next/link";
import { Button } from "../core/Button";
import { HeaderTopDates } from "./HeaderTopDates";

export const HeaderTop = () => {
  return (
    <div className="bg-[#f5f8fd] lg:block hidden">
      <div className="container">
        <div className="flex items-center justify-between">
          <div className="flex gap-4">
            <div className="flex items-end gap-2 text-base">
              <span className="text-xl">
                <PhoneIcon />
              </span>
              Phone
              <Link href={"tel:+97685050500"} className="hover:text-primary hover:underline duration-300">
                +976 85050500
              </Link>
            </div>
            <div className="flex items-end gap-2 text-base">
              <span className="text-xl">
                <MapMarker />
              </span>
              Location
              <Link target="_blank" href={"https://maps.app.goo.gl/mtShXuLmNqay8jg76"} className="hover:text-primary hover:underline duration-300">
                Ulaanbaatar, Mongolia
              </Link>
            </div>
            <div className="flex items-end gap-2 text-base">
              <span className="text-xl">
                <BusinessTime />
              </span>
              July 8 to 9, 2024
            </div>
            <div className="flex items-end gap-2 text-base">
              <HeaderTopDates />
            </div>
          </div>
          <div className="flex items-center gap-2">
            <Button as={Link} href="/register" color="blue">
              Register
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};
