"use client";
import { useEffect, useRef, useState } from "react";

const dates = [
  "Abstract Submission Deadline- June 23, 2024",
  "Registration and Payment Deadline- July 1, 2024",
  "Start of The Conference- July 8, 2024",
];
export const HeaderTopDates = () => {
  const [date, setDate] = useState(dates[0]);
  const intervalRef = useRef<null | NodeJS.Timeout>(null);
  useEffect(() => {
    let i = 0;
    intervalRef.current = setInterval(() => {
      setDate(dates[i]);
      i = i === dates.length - 1 ? 0 : i + 1;
    }, 3000);
    return () => {
      if (intervalRef.current) clearInterval(intervalRef.current);
    };
  }, []);
  return <span>{date}</span>;
};
