import Link from "next/link";
import { HeaderTop } from "./HeaderTop";
import Image from "next/image";
import { nanoid } from "nanoid";
import { Button } from "../core/Button";
import { MobileNav } from "./MobileNav";
import { menus } from "@nano/utils/menus";
import { headers } from "next/headers";

export const Header = () => {
  const headersList = headers();
  const domain = headersList.get("host") || "";
  const fullUrl = headersList.get("referer") || "";
  const pathName = fullUrl.split(domain)[1];
  return (
    <div>
      <div className="lg:h-[122px] h-[74px]" />
      <header className="fixed left-0 top-0 right-0 z-10">
        <HeaderTop />
        <div className="bg-btn-accent sticky shadow-lg">
          <div className="container">
            <div className="flex relative py-3 justify-between items-stretch">
              <div>
                <Link href={"/"}>
                  <Image src={"/logo.png"} width={50} height={50} alt="Noma Tech" />
                </Link>
              </div>
              <nav className="lg:block hidden">
                <ul className="flex gap-4 h-full">
                  {menus.map((menu) => (
                    <li key={nanoid()} className="h-full flex items-center">
                      <div className="dropdown dropdown-hover">
                        {menu.name === "Brochure" ? (
                          <Link
                            target="_blank"
                            className={`flex text-white h-full items-center font-medium ${pathName === menu.link && "text-[#A59281]"}`}
                            href={"/brochure-final.pdf"}
                            download={"/brochure-final.pdf"}
                          >
                            {menu.name}
                          </Link>
                        ) : menu.name === "Program" ? (
                          <Link
                            target="_blank"
                            className={`flex text-white h-full items-center font-medium ${pathName === menu.link && "text-[#A59281]"}`}
                            href={"/agenda.pdf"}
                            download={"/agenda.pdf"}
                          >
                            {menu.name}
                          </Link>
                        ) : (
                          <Link
                            className={`flex text-white h-full items-center font-medium ${pathName === menu.link && "text-[#A59281]"}`}
                            href={menu.link}
                          >
                            {menu.name}
                          </Link>
                        )}
                        {menu.children && (
                          <ul tabIndex={0} className="header-dropdown dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52">
                            {menu.children.map((child) => (
                              <li key={nanoid()}>
                                <Link href={child.link}>{child.name}</Link>
                              </li>
                            ))}
                          </ul>
                        )}
                      </div>
                    </li>
                  ))}
                </ul>
              </nav>
              <div className="md:block hidden">
                <Button color="black" as={Link} href="/noma.pdf" download="/noma.pdf" target="_blank">
                  Download abstracts
                </Button>
              </div>
              <MobileNav />
            </div>
          </div>
        </div>
      </header>
    </div>
  );
};
